public class TasksCreation {
	public static Task createTask(String sub,Id whatID,Id OwnerId)
    {
        Task t = new Task();
        t.Subject = sub;
        t.WhatId = whatID;
        t.OwnerId = OwnerId;
        t.Status = 'In Progress';
        t.Description = ''+sub+' Task Created';
        return t;
    }
}
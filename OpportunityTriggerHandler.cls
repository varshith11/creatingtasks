public class OpportunityTriggerHandler {
    private static String OPPORTUNITY_STATUS_NEED_ANALYSIS = 'Needs Analysis';	
	public static void createTasks(List<Opportunity> oppList,Map<Id,Opportunity> oppMap)
    {
        List<Task> insertTaskList =new List<Task>();
        Map<Id,List<Task>> oppIdToTasks = new Map<Id,List<Task>>(); 
        List<Task> taskList = new List<Task>();
        List<Id> oppIds = new List<Id>();
        for(Opportunity opp : oppList)
        {
            if(opp.StageName.equalsIgnoreCase(OPPORTUNITY_STATUS_NEED_ANALYSIS))
            {
                oppIds.add(opp.Id);
            }
        }
        if(oppIds.size()>0)
        {
            for(Task t : [SELECT Id,WhatID,Ownerid,subject,Status FROM Task WHERE WhatID IN : oppIds AND Status ='Open'])
            {
                if(!oppIdToTasks.containsKey(t.WhatID))
                {
                    oppIdToTasks.put(t.WhatID,new List<Task>());
                }
                oppIdToTasks.get(t.WhatID).add(t);
            }
        }
        	   
        for(Opportunity opp:oppList)
        {
            if(opp.StageName.equalsIgnoreCase(OPPORTUNITY_STATUS_NEED_ANALYSIS))
            {
                if(oppIdToTasks.size()>0 )
                {
                    for(Tast_Subject__c TS : Tast_Subject__c.getall().values())
                    {
                        Boolean found = false;
                        for(Task t : oppIdToTasks.get(opp.Id))
                        {
                            if(t.Subject.equalsIgnoreCase(TS.Subject__c))
                            {
                                found = true;
							}
                        }
                        if(!found)
                        		insertTaskList.add(TasksCreation.createTask(TS.Subject__c,opp.Id,opp.OwnerId));
                        found = false;
                    }   
                }
                else
                {
                    for(Tast_Subject__c TS : Tast_Subject__c.getall().values())
                    {
                        insertTaskList.add(TasksCreation.createTask(TS.Subject__c,opp.Id,opp.OwnerId));
                    }
                }
            } 
		}
        System.debug(insertTaskList);
        if(insertTaskList.size()> 0)
        {
            insert insertTaskList;
        }
    }
    @AuraEnabled
    public static void createTaskUsingAction(Id recordId)
    {
        List<Opportunity> opp = new List<Opportunity>();
        opp = [SELECT Id,StageName FROM Opportunity WHERE Id =: recordId];
        opp[0].StageName = 'Needs Analysis';
        update opp;
    }
}
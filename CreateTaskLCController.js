({
	doInit : function(component, event, helper) {
		var action = component.get('c.createTaskUsingAction');
        action.setParams({
            "recordId" : component.get('v.recordId') 
        });
        action.setCallback(this, function(a){
            var state = a.getState(); // get the response state
            if(state == 'SUCCESS') {
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "title": "Success!",
                    "message": "Created Tasks"
                });
                toastEvent.fire();
            }
        });
        $A.enqueueAction(action);
	}
})